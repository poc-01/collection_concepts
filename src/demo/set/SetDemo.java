package demo.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

public class SetDemo {

    public static void main(String[] args) {
       // HashSet<String> set = new HashSet<String>();  //sorted version of set  // albaphetically sort
        TreeSet<String> set = new TreeSet<String>();
        set.add("bala");
        set.add("pal");
        set.add("andrew");
        set.add("bala");
        System.out.println(set);
        Iterator<String> itr = set.iterator();
        while (itr.hasNext()){
            String str = itr.next();
            System.out.println(str);
        }
        set.remove("pal");  //remove
        System.out.println(set); //After remove set
        if(set.contains("bala")){
            System.out.println("contains bala");
        }
        else {
            System.out.println("not contains bala");
        }
    }
}
