package demo.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class ArrayListConcepts {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        ArrayList<Integer> arrayList1 = new ArrayList<Integer>();
        arrayList.add(10);
        arrayList.add(20);
        arrayList.add(30);
        arrayList.add(40);
        System.out.println(arrayList);
        arrayList.remove(1);
        System.out.println(arrayList);
        //add element
        for (int i = 0; i < 4; i++) {
            arrayList1.add(i);
        }
        System.out.println(arrayList1);
        //get element
        System.out.println(arrayList1.get(1));
        //update element
        arrayList1.set(1, 25);
        System.out.println(arrayList1);
        //contains method
        if (arrayList1.contains(3)) {
            System.out.println("list contains");
        } else {
            System.out.println("list not contains");
        }
        //for loop iteration
        System.out.println("for loop iteration arraylist");
        for (int j = 0; j < arrayList.size(); j++) {
            System.out.println(arrayList.get(j));
        }
        //for each loop
        System.out.println("for each loop iteration arraylist");
        for (int k : arrayList) {
            System.out.println(k);
        }
        //iteration
        System.out.println("Using Iterator");
        Iterator<Integer> itr = arrayList.iterator();
        // int a= itr.next();
        //  System.out.println(a);
        System.out.println("Using Iterator loop");
        while (itr.hasNext()) {
            int b = itr.next();
            System.out.println(b);

        }

    }
}
