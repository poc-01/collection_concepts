package demo.queue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueDemo {

    public static void main(String[] args) {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<Integer>();
        Queue<Integer> queue = new LinkedList<>();
        for(int i=10;i>0;i--){
         queue.add(i);
         //priorityQueue.add(i);
        }

        System.out.println(queue);
        //find head
        System.out.println( queue.peek());

        //after remove
    queue.poll();
        System.out.println( queue.peek());
        //System.out.println(priorityQueue);
    }
}
